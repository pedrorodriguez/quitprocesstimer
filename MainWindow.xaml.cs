﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace QuitProcessTimer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TimerButton_Click(object sender, RoutedEventArgs e)
        {
            MaxTimeLabel.Content = Convert.ToInt32(TimerValue.Text);
            ProcessQuitter pq = new ProcessQuitter() { TimerValue = Convert.ToInt32(TimerValue.Text), ProcessName = ProcessName.Text, progressBar = TimerProgress};
            Thread t = new Thread(new ThreadStart(pq.KillThreadOnTime));
            t.Start();
        }
    }
    public class ProcessQuitter
    {
        public int TimerValue { get; set; }
        public string ProcessName { get; set; }
        public System.Windows.Controls.ProgressBar progressBar { get; set; }
        public void KillThreadOnTime()
        {
            int timerValue = TimerValue;
            Thread pThread = new Thread(
                new System.Threading.ThreadStart(
                    delegate()
                        {
                            progressBar.Dispatcher.Invoke(
                                System.Windows.Threading.DispatcherPriority.Normal,
                                new Action(
                                    delegate()
                                        {
                                            progressBar.Maximum = timerValue;
                                        }));
                        }));
            pThread.Start();
            while (timerValue > 0)
            {
                TimeSpan t = new TimeSpan(0, 1, 0);
                Thread.Sleep(t);
                timerValue--;
                Thread pThread2 = new Thread(
                new System.Threading.ThreadStart(
                    delegate()
                    {
                        progressBar.Dispatcher.Invoke(
                            System.Windows.Threading.DispatcherPriority.Normal,
                            new Action(
                                delegate()
                                {
                                    progressBar.Value++;
                                }));
                    }));
                pThread2.Start();
            }
            Process[] procs = Process.GetProcessesByName(ProcessName);
            foreach (Process proc in procs)
            {
                proc.Kill();
            }
        }
    }
}
